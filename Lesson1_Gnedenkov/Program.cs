﻿using System;

namespace Lesson1_Gnedenkov
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите первую цифру");
            var a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите вторую цифру");
            var b = Convert.ToInt32(Console.ReadLine());
            var x = a + b;
            Console.WriteLine(x);
        }
    }
}